describe('Resource content and archives', () => {
    it('can show content', () => {
        const myinput = '"Test record 12"';
        const expectedResultCount = 1;
        
        cy.visit('/');

        cy.get('input[name="query"]').type(myinput).should('have.value', myinput);
        cy.get('button[name="searchSubmit"]').click();

		//wait for search term to appear in a span.query-item-label
		cy.get('span.query-item-label').contains(myinput, { timeout: 10000 }).should('be.visible');

        cy.get('div .searchresultitem').should('have.length', expectedResultCount);
    })

})
