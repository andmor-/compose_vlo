#!/usr/bin/env bash
for i in $(seq 1 20); do 
	TARGET_FILE="doc${i}.json"
	echo "Creating ${TARGET_FILE}"
	sed -e 's/{{x}}/'${i}'/g' docX.json.template > "${TARGET_FILE}";
done
echo "Done"

