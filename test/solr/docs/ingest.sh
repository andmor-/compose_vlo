#!/usr/bin/env bash
set -e

SOLR_HANDLER_URL="${SOLR_HANDLER_URL:-https://localhost/solr/vlo-index/update}"
FILE_NAME="$1"

SOLR_USER="${VLO_DOCKER_SOLR_ADMIN_USER:-user_admin}"
SOLR_PASSWORD="${VLO_DOCKER_SOLR_PASSWORD_ADMIN:?Error - SOLR_PASSWORD not set}"

if ! [ -e "${FILE_NAME}" ]; then
	echo "Error: file not found ${FILE_NAME}"
	exit 1
fi

echo 'Connected to solr'

curl -k \
	--retry 12 --retry-delay 5 --retry-all-errors \
	-u "${SOLR_USER}:${SOLR_PASSWORD}" \
	-X POST \
	-H 'Content-Type: application/json' \
	--data-binary "@${FILE_NAME}" \
	"${SOLR_HANDLER_URL}/json/docs?commit=true"
