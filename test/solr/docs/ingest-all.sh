#!/usr/bin/env bash
SCRIPT_BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

VLO_DOCKER_SOLR_PASSWORD_ADMIN="${VLO_DOCKER_SOLR_PASSWORD_ADMIN:?Error VLO_DOCKER_SOLR_PASSWORD_ADMIN not set}"

# Wait for solr
echo "Waiting for Solr..."
while ! nc -z vlo-solr 8983; do   
  sleep 0.1 # wait for 1/10 of the second before check again
done

export VLO_DOCKER_SOLR_PASSWORD_ADMIN
find "${SCRIPT_BASE_DIR}" -maxdepth 1 -name '*.json' -print -exec bash ${SCRIPT_BASE_DIR}/ingest.sh {} \;
